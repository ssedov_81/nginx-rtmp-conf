# RTMP Restream (nginx.conf)
## Описание
Файл конфигурации сервера nginx с модулем rtmp для ретрансляции одного входящего rtmp потока от клиента на несколько исходящих rtmp потоков на различные IP адреса. Управление доступом к ретрансляции и управление переадрессацией rtmp птоков осуществляется динамически через механизм rtmp модуля notify путем отправки http запросов на rtmp notify сервер.  
Является частью проекта RTMP Restream сервиса.

## Запуск nginx-rtmp сервера в docker контейнере
docker run -d -p 1935:1935 -p 80:80 --name nginx-rtmp -v $(pwd)/nginx.conf:/etc/nginx/nginx.conf.template -e NOTIFY_FQDN=rtmp.notify.server alfg/nginx-rtmp
